# Projective texture method in high-definition human reconstruction

This project uses projective texture method to reconstruct high definition human.  
The original human model is produced by Multi-view PIFu (ICCV 2019).  
With our method, we can reconstruct the human texture with multi-view RGB images, and achieve realtime performance.

For more information, please checkout the following post:

![](https://i.imgur.com/ZcU3Xz1.png)
