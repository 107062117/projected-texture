﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ViewCapture : MonoBehaviour
{
    private Camera Cam;
    public string folder;
    public string FileCounter;
    // Use this for initialization
    
    void CamCapture()
    {
        RenderTexture currentRT = RenderTexture.active;
        RenderTexture.active = Cam.targetTexture;

        Cam.Render();

        Texture2D Image = new Texture2D(Cam.targetTexture.width, Cam.targetTexture.height);
        Image.ReadPixels(new Rect(0, 0, Cam.targetTexture.width, Cam.targetTexture.height), 0, 0);
        Image.Apply();
        RenderTexture.active = currentRT;

        var Bytes = Image.EncodeToPNG();
        Destroy(Image);

        File.WriteAllBytes("/Users/licairong/Desktop/"+folder+"/" + FileCounter + ".png", Bytes);
        Debug.Log("Catch"+FileCounter);
    }
    private void Awake()
    {
        Cam = GetComponent<Camera>();
    }
    
    void Update() 
    {
        if (Input.GetKeyDown(KeyCode.Space))
            CamCapture();
    }
}