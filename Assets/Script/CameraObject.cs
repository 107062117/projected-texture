using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//[ExecuteInEditMode]
public class CameraObject : MonoBehaviour
{
    public Material material;
    public Camera camera;
    public Transform transform;
    public RenderTexture depthTex;
    public Texture2D[] textures;
    public Texture2D tex;
    private int frameCount = 0;
    private int frameLength = 0;
    private void OnEnable()
    {
        camera = GetComponent<Camera>();
        camera.depthTextureMode = DepthTextureMode.Depth;
        camera.SetReplacementShader(Shader.Find("Custom/Render Depth"), "");
        transform = gameObject.transform;
        depthTex = camera.targetTexture;
        tex = textures[0];
        frameLength = textures.Length;
    }
    void OnRenderImage(RenderTexture src, RenderTexture des)
    {
        Graphics.Blit(src, des, material);
    }
    
    void FixedUpdate() 
    {
        // update project texture //
        tex = textures[frameCount];
        frameCount++;
        if (frameCount == frameLength)
        {
            frameCount = 0;
        }
    }
}
