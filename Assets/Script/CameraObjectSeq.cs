﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraObjectSeq : MonoBehaviour
{
    public Material material;
    public Camera camera;
    public Transform transform;
    public RenderTexture depthTex;
    public Matrix4x4 extrin;
    public RenderTexture[] textures;
    public RenderTexture tex;
    private int frameCount = 0;
    private int frameLength = 0;
    private void OnEnable()
    {
        camera = GetComponent<Camera>();
        camera.depthTextureMode = DepthTextureMode.Depth;
        camera.SetReplacementShader(Shader.Find("Custom/Render Depth"), "");
        transform = gameObject.transform;
        depthTex = camera.targetTexture;
        tex = textures[0];
        frameLength = textures.Length;
    }
    void OnRenderImage(RenderTexture src, RenderTexture des)
    {
        Graphics.Blit(src, des, material);
    }

    void extrinsicToCameraPos()
    {
        extrin.SetRow(2, new Vector4(-extrin[2, 0], -extrin[2, 1], -extrin[2, 2], -extrin[2, 3]));
        Matrix4x4 view = extrin;
        view = view.inverse;

        Vector3 pos = new Vector4(view[0, 3], view[1, 3], view[2, 3]);
        Vector3 forward = new Vector3(view[0, 2], view[1, 2], view[2, 2]);
        transform.position = pos;
        transform.LookAt(pos+forward);
    }
    
    // void FixedUpdate() 
    // {
    //     tex = textures[frameCount];
    //     frameCount++;
    //     if (frameCount == frameLength)
    //     {
    //         frameCount = 0;
    //     }
    // }
}
