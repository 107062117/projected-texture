﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class multiProjectSeq : MonoBehaviour
{
    // Start is called before the first frame update
    public Material material;
    private List<CameraObjectSeq> cameraObjects = new List<CameraObjectSeq>();
    private List<Matrix4x4> projMatList = new List<Matrix4x4>();
    private List<Matrix4x4> viewMatList = new List<Matrix4x4>();
    private List<Vector4> normList = new List<Vector4>();
    public Transform[] cams;
    
    private int front = -1;

    private void OnEnable()
    {
        Time.fixedDeltaTime = 1/60f;
        cameraObjects.Clear();
        projMatList.Clear();
        viewMatList.Clear();
        normList.Clear();
        // get all camera
        float maxScore = -1;
        int i = 0;
        foreach (Transform child in cams)
        {
            CameraObjectSeq cameraObject = child.gameObject.GetComponent<CameraObjectSeq>();
            if (cameraObject)
            {
                cameraObjects.Add(cameraObject);
                Matrix4x4 matProj = cameraObject.camera.projectionMatrix;
                Matrix4x4 matView = Matrix4x4.identity;
                matView = Matrix4x4.TRS(Vector3.zero, cameraObject.transform.rotation, Vector3.one);
                matView = matView.transpose;
                matView = matView * Matrix4x4.Translate(cameraObject.transform.position * -1);
                Matrix4x4 projMatrix =  matProj * matView;
                projMatList.Add(projMatrix);
                viewMatList.Add(matView);
                // compute camera normal
                Vector4 normal = new Vector4(cameraObject.transform.forward.x, cameraObject.transform.forward.y, cameraObject.transform.forward.z, 0) * -1;
                normList.Add(normal);
                
                // set depth texture
                material.SetTexture("_DepthArr"+i.ToString(), cameraObject.depthTex);
                material.SetTexture("_TexArr"+i.ToString(), cameraObject.tex);
                // find front camera
                float score = Vector3.Dot(-cameraObject.transform.forward, transform.forward);
                if (score > maxScore)
                {
                    maxScore = score;
                    front = i;
                }
                i++;
            }
        }
        // set camera views
        material.SetFloat("_View", cameraObjects.Count);
        material.SetInt("_Front", front);
        material.SetMatrixArray("_ProMatList", projMatList);
        material.SetMatrixArray("_ViewMatList", viewMatList);
        material.SetVectorArray("_NormList", normList);
    }

    // Update is called once per frame
    private void setMatrix()
    {
        for (int i = 0; i < cameraObjects.Count; i++)
        {
            CameraObjectSeq camObj = cameraObjects[i];
            Camera camera = camObj.camera;
            if (camera != null)
            {
                //set view / proj matrix
                //Matrix4x4 matProj = Matrix4x4.Ortho(-128, 128, -128, 128, 0.1f, 200);
                Matrix4x4 matProj = camera.projectionMatrix;
                Matrix4x4 matView = Matrix4x4.identity;
                matView = Matrix4x4.TRS(Vector3.zero, camObj.transform.rotation, Vector3.one);
                matView = matView.transpose;
                matView = matView * Matrix4x4.Translate(camObj.transform.position * -1);
                Matrix4x4 projMatrix =  matProj * matView;
                projMatList[i] = projMatrix;
                viewMatList[i] =  matView;
                
                // compute camera normal
                Vector4 normal = new Vector4(camObj.transform.forward.x, camObj.transform.forward.y, camObj.transform.forward.z, 0) * -1;
                normList[i] = normal;
                
                // set depth texture
                material.SetTexture("_DepthArr"+i.ToString(), camObj.depthTex);
                material.SetTexture("_TexArr"+i.ToString(), camObj.tex);
            }
        }
        material.SetMatrixArray("_ProMatList", projMatList);
        material.SetMatrixArray("_ViewMatList", viewMatList);
        material.SetVectorArray("_NormList", normList);
    }
    void Update(){
        
        setMatrix();
    }
}
