﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Render Depth" {
    Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 100
        Pass {
            CGPROGRAM

            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"


            struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

			sampler2D _MainTex;
			sampler2D _CameraDepthTexture;
			float4 _MainTex_ST;
            
            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);

                // Compute texture coordinate
                //o.screenPos = ComputeScreenPos(o.vertex);
                o.uv = v.uv;
                return o;
            }

            half4 frag(v2f i) : SV_Target
            {
                // Camera parameters
                float near = _ProjectionParams.y;
                float far = _ProjectionParams.z;

                // Sample the depth texture to get the linear depth
                float rawDepth = UNITY_SAMPLE_DEPTH(tex2D(_CameraDepthTexture, i.uv));
                float ortho = (far - near) * (1 - rawDepth) + near;
                //float depth = LinearEyeDepth(rawDepth);
                float depth = lerp(LinearEyeDepth(rawDepth), ortho, unity_OrthoParams.w) / far;
                half4 c;
                c.r = depth;
                c.g = depth;
                c.b = depth;
                c.a = 1;

                return c;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}