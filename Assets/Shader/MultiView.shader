﻿Shader "Projector/multiView"
{
    Properties {
        _Texture ("Texture", 2D) = "white" {}
        _Angle("Angle", Range(0, 1)) = 0.1
        _DepthDiff("DepthDiff", Range(0, 1)) = 0.01
	}
	Subshader {
		Tags {"RenderType"="Opaque"}
		Pass {
        	Blend SrcAlpha OneMinusSrcAlpha
			Offset -1, -1 //越小的越晚render

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

            // input param
            float4x4 unity_Projector;
			float4x4 unity_ProjectorClip;
            sampler2D _Texture;
            uniform sampler2D _TexArr0;
            uniform sampler2D _TexArr1;
            uniform sampler2D _TexArr2;
            uniform sampler2D _TexArr3;
            uniform sampler2D _TexArr4;
            uniform sampler2D _TexArr5;
            uniform sampler2D _TexArr6;
            uniform sampler2D _DepthArr0;
            uniform sampler2D _DepthArr1;
            uniform sampler2D _DepthArr2;
            uniform sampler2D _DepthArr3;
            uniform sampler2D _DepthArr4;
            uniform sampler2D _DepthArr5;
            uniform sampler2D _DepthArr6;
            uniform int _Front;
            fixed _Angle;
            fixed _DepthDiff;
            uniform float4x4 _PVMatList[7];
            uniform float4x4 _ViewMatList[7];
            uniform float4 _NormList[7];
            uniform float _View;


            // vertex shader io structure
            struct appdata
            {
                float4 vertex : POSITION; //取得頂點位置，position:變數含義
                float3 normal : NORMAL; // normal
                float2 uv : TEXCOORD0;
            };

			struct v2f {
				float2 uv : TEXCOORD0;
				float4 pos : SV_POSITION;
                float3 worldNormal : TEXCOORD2;
                float4 uvShadow[7]: TEXCOORD3;
			};

            float4 getColor(float i, float2 uv)
            {
                switch(i)
                {
                    case 0:
                        return tex2D(_TexArr0, uv);
                        break;
                    case 1:
                        return tex2D(_TexArr1, uv);
                        break;
                    case 2:
                        return tex2D(_TexArr2, uv);
                        break;
                    case 3:
                        return tex2D(_TexArr3, uv);
                        break;
                    case 4:
                        return tex2D(_TexArr4, uv);
                        break;
                    case 5:
                        return tex2D(_TexArr5, uv);
                        break;
                    case 6:
                        return tex2D(_TexArr6, uv);
                        break;
                    default:
                        return tex2D(_TexArr0, uv);
                        break;
                }
            }

            float getDepth(float i, float2 uv)
            {
                switch(i)
                {
                    case 0:
                        return tex2D(_DepthArr0, uv);
                        break;
                    case 1:
                        return tex2D(_DepthArr1, uv);
                        break;
                    case 2:
                        return tex2D(_DepthArr2, uv);
                        break;
                    case 3:
                        return tex2D(_DepthArr3, uv);
                        break;
                    case 4:
                        return tex2D(_DepthArr4, uv);
                        break;
                    case 5:
                        return tex2D(_DepthArr5, uv);
                        break;
                    case 6:
                        return tex2D(_DepthArr6, uv);
                        break;
                    default:
                        return tex2D(_DepthArr0, uv);;
                        break;
                }
            }


            // View chosen view by different color
            float4 ViewColor(float3 uvS[10], float2 uv, float3 worldNormal)
            {
                // average
                float4 colorList[6] = {
                    float4(1, 0, 0, 1), 
                    float4(0, 1, 0, 1), 
                    float4(0, 0, 1, 1), 
                    float4(1, 0, 1, 1),
                    float4(1, 1, 0, 1),
                    float4(0, 1, 1, 1)
                };

                // normal test
                float4 color = float4(0, 0, 0, 0);
                float total = 0;
                

                for (int index = 0; index < 4; index ++)
                {
                    float score = dot(worldNormal, _NormList[index]) / length(worldNormal) / 1;
                    float depth = getDepth(index, uvS[index].xy);
                    if (!(score < _Angle|| abs(uvS[index].z - depth) > _DepthDiff))
                    {
                        // check if over project
                        float4 c = getColor(index, uvS[index].xy);
                        if (c.a != 1) continue;
                        else {
                            if (index ==  _Front) score *= 3;
                            total += score;
                            color += score * colorList[index];
                        }
                    }
                }
                if (total != 0)
                {
                    color = color / total;
                    return color;
                }
                return tex2D(_Texture, uv);
            }

            // Draw texture color
            float4 DrawTexture(float3 uvS[10], float2 uv, float3 worldNormal)
            {
                float4 color = float4(0, 0, 0, 0);
                float total = 0;

                for (int index = 0; index < _View; index ++)
                {
                    // 內積 (vertex normal, camera front) 找出vertex normal 和 camera 的夾角，當成投影顏色的比重
                    float score = dot(worldNormal, _NormList[index]) / length(worldNormal) / 1;
                    // 用vertex投影位置在depth texture上找到該點的深度，如果和該點原本的深度不同，該點視為self occlusion
                    float depth = getDepth(index, uvS[index].xy);

                    //當vertex normal和camera投影的夾角小於一定值，且該點不是self occlusion
                    if (!(score < _Angle || abs(uvS[index].z - depth) > _DepthDiff))
                    {
                        // 用projective texture在該點投影位置sample顏色
                        float4 c = getColor(index, uvS[index].xy);
                        // 如果sample到空白的地方，不計算這個view
                        if (c.a != 1) continue;
                        else {
                            // 提高正面的比重，把顏色和比重加總，之後加權平均
                            if (index ==  _Front) score *= 3;
                            total += score;
                            color += c * score;
                        }
                    }
                }
                if (total != 0)
                {
                    color = color / total;
                    return color;
                }
                // 如果沒有任何view可以投影到這個vertex上，該點顏色用pifu顏色取代
                return tex2D(_Texture, uv);
            }


			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                float4 world = mul(unity_ObjectToWorld, v.vertex);
                for (int i = 0; i < _View; i++){
                    o.uvShadow[i] = mul(_PVMatList[i], world);
                    o.uvShadow[i].z = mul(_ViewMatList[i], world).z / 2;
                }
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{   
                //ncd : [-1, 1]
                float3 uv[10];
                for (int j = 0; j < _View; j++)
                {
                    float2 ndc = float2(i.uvShadow[j].x/i.uvShadow[j].w, i.uvShadow[j].y/i.uvShadow[j].w);
                    uv[j] = float3((1 + ndc.x) * 0.5, (1 + ndc.y) * 0.5, i.uvShadow[j].z);
                }
                return DrawTexture(uv, i.uv, i.worldNormal);
			}
			ENDCG
		}
	}
    FallBack "Diffuse"
}
